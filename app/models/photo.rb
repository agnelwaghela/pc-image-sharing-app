class Photo < ActiveRecord::Base
  belongs_to :album
  belongs_to :user
  validates :user_id, presence: true
  validates :tag_line, length: { maximum: 150 }
  mount_uploader :image, ImageUploader
  
end