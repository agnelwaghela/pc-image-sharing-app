class Album < ActiveRecord::Base
  belongs_to :user
  has_many :photos, dependent: :destroy
  validates :name, presence: true, length: { minimum: 3, maximum: 80 }
  validates :user_id, presence: true
end