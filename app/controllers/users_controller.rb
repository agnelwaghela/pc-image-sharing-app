class UsersController < ApplicationController
  
  before_action :set_user, only: [:edit, :update, :show, :destroy]
  before_action :require_user, only: [:index, :edit, :show, :destroy]
  before_action :require_same_user, only: [:edit, :update, :destroy]
  
  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.create(user_params)
    if @user.save
      # TODO store the user_id in session for future use
      flash[:success] = 'Your account has been successfully created! Please Log in.'
      redirect_to home_path
    else
      render :new
    end
  end
  
  def edit
    
  end
  
  def update
    if @user.update(user_params)
      flash[:success] = 'Your profile has been updated successfully!'
      redirect_to @user
    else
      render :edit
    end
  end
  
  def show
    if current_user == @user
      @albums = @user.albums.paginate(page: params[:page], per_page: 10)
    else
      @albums = @user.albums.where(private: false).paginate(page: params[:page], per_page: 10)
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:danger] = "Account Deleted!"
    redirect_to home_path
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
    
    def set_user
      @user = User.find(params[:id])
    end
    
    def require_same_user
      if current_user != @user
        flash[:danger] = "You can only edit your own profile"
        redirect_to home_path
      end
    end
  
end