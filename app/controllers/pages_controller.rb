class PagesController   < ApplicationController
  
  def home
    public_albums = Album.where(private: false)
    @photos = Photo.where('album_id IN (?)', public_albums.ids).order('created_at desc').limit(25)
  end
  
end