class PhotosController < ApplicationController
  
  before_action :set_album
  before_action :set_photo, only: [:show, :edit, :update, :destroy]
  before_action :require_user, except: [:show]
  before_action :require_same_user, only: [:edit, :update, :destroy]
  
  def new
    @photo = Photo.new
  end
  
  def create
    if @album.photos.count <= 25 
      @photo = Photo.new(photo_params)
      @photo.album = @album
      @photo.image = params[:file]
      @photo.user = current_user
      @photo.save!
      render json: @photo
    else
      render json: { error: 'You have reached the limit of 25 images per album.' }
    end
  end
  
  def show
    
  end
  
  def edit
  
  end
  
  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { 
          flash[:success] = 'Image details saved!'
          redirect_to edit_album_photo_path(@album, @photo)
        }
        format.json { render :show, status: :ok, location: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @photo.destroy
    redirect_to :back
  end
  
  private
  
    def set_album
      @album = Album.find(params[:album_id])
    end
    
    def set_photo
      @photo = Photo.find(params[:id])
    end
    
    def photo_params
      params.require(:photo).permit(:album_id, :image, :tag_line)
    end
    
    def require_same_user
      if current_user != @photo.user
        flash[:danger] = 'You can only edit your own photos!'
        redirect_to home_path
      end
    end
  
end