class AlbumsController < ApplicationController
  
  before_action :set_album, only: [:edit, :update, :show, :destroy]
  before_action :require_user, except: [:show, :browse]
  before_action :require_same_user, only: [:edit, :update, :destroy]
  
  def index
    @albums = Album.where(user: current_user).paginate(page: params[:page], per_page: 5)
  end
  
  def new
    @album = Album.new
  end
  
  def create
    @album = Album.create(album_params)
    @album.user = current_user
    if @album.save
      flash[:success] = 'Album Created!'
      redirect_to album_path(@album) # TO DO change to show album page
    else
      render :new
    end
  end
  
  def edit
    
  end
  
  def update
    if @album.update(album_params)
      flash[:success] = "Album Details Updated"
      redirect_to album_path(@album)
    else
      render :edit
    end
  end
  
  def show
    
  end
  
  def destroy
    @album.destroy
    flash[:success] = 'Album Deleted!'
    redirect_to albums_path
  end
  
  def browse
    @albums = Album.where(private: false).order('created_at desc').paginate(page: params[:page], per_page: 25)
    respond_to do |format|
      format.html
      format.json { render json: @albums }
    end
  end
  
  private
  
    def album_params
      params.require(:album).permit(:name, :private)
    end
    
    def set_album
      @album = Album.find(params[:id])
    end
    
    def require_same_user
      if current_user != @album.user
        flash[:danger] = "You can edit only your albums!"
        redirect_to albums_path
      end
    end
  
end