module ApplicationHelper
  
  def gravatar_for( user, options = { size: 80, class: '' } )
    gravatar_id = Digest::MD5::hexdigest( user.email )
    size = options[:size]
    classes = options[:class]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag( gravatar_url, alt: user.name, class: 'gravatar ' + classes )
  end
  
end

module Possessive
  def possessive
    suffix = if self.downcase == 'it'
      "s"
    elsif self.downcase == 'who'
      'se'
    elsif self.end_with?('s')
      "'"
    else
      "'s"
    end
    self + suffix
  end
end

class String
  include Possessive
end
