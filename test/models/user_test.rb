require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: 'Jimish', email: 'jimish@pcisa.com', password: 'jimish')
  end
  
  test "user should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 41
    assert_not @user.valid?
  end
  
  test "name should not be too short" do
    @user.name = "aa"
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end
  
  test "email length should be within bounds" do
    @user.email = "a" * 100 + "@pcisa.com"
    assert_not @user.valid?
  end
  
  test "email should be unique" do
    dup_user = @user.dup
    dup_user.email = @user.name.upcase
    @user.save
    assert_not dup_user.valid?
  end
  
  test "email validation should accept valid email address" do
    valid_addresses = %w|user@eee.com R_TDD@eee.hello.org user@example.com jimish@desidime.com mehul@desidime.com i+am@monk.com|
    valid_addresses.each do |va|
      @user.email = va
      assert @user.valid?
    end
  end
  
  test "email validation should reject invalid email address" do
    invalid_addresses = %w|user_at_eee.com user@example,com jimish_a_desidime.com mehuldesidime.com iam@a+monk.com|
    invalid_addresses.each do |va|
      @user.email = va
      assert_not @user.valid?
    end
  end
end