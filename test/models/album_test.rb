require 'test_helper'

class AlbumTest < ActiveSupport::TestCase
  
  def setup
    @user = User.create(name: 'Agnel', email: 'agnel@github.com', password: 'agnel')
    @album = @user.albums.build(name: '1st Birthday Collection')
  end
  
  test "album should have a name" do
    @album.name = " "
    assert_not @album.valid?
  end
  
  test "album name should not be too long" do
    @album.name = 'a' * 100
    assert_not @album.valid?
  end
  
  test "album name should not be too short" do
    @album.name = 'aa'
    assert_not @album.valid?
  end
  
  test "should belong to a user" do
    @album.user_id = nil
    assert_not @album.valid?
  end
end