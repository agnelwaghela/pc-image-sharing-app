Parity Cube Image Sharing App
-----------------------------

This Assignment covers - 

Ruby on Rails 4 Image sharing app.

Users
1. will be able to sign up on the website via their email address 
2. can create their own personal albums 
3. can upload upto 25 images per album 
4. can create any number of albums 
5. Images uploaded would also be visible on the home page of the website 

All users/visitors can browse any album. 

Album would have
1. Name of the Album 
2. Created Date 
3. Owner 
  
Photos would have 
1. Image 
2. Tag line for photo 
3. Created date 
4. owner 

Home page of the website 
1. display the last 25 images uploaded and who uploaded it. 

User profile page 
1. display all the albums by that user.. 

Use any database of your choice
Please also provide a brief README
Not suppose to spend too much time on UI/UX elements and use twitter bootstrap to make life simpler
**Ensure models have appropriate validations required.**