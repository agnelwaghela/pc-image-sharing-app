class AddPrivateToAlbum < ActiveRecord::Migration
  def change
    add_column :albums, :private, :boolean
  end
end
