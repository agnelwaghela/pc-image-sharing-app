class ChangeColumnDefault < ActiveRecord::Migration
  def change
    change_column :albums, :private, :boolean, :default => false
  end
end
