# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

mehul = User.create(name: 'Mehul', email: 'mehul@pcisa.com', password: 'mehul')
jimish = User.create(name: 'Jimish', email: 'jimish@pcisa.com', password: 'jimish')
agnel = User.create(name: 'Agnel Waghela', email: 'agnelwaghela@gmail.com', password: 'agnel')

Album.create(name: '1st Birthday Celebration', user: mehul, private: true)
Album.create(name: '10th Farewell', user: mehul) # default private is false
Album.create(name: 'Parity Cube 1st Anniversary', user: jimish)
Album.create(name: 'Wedding Day', user: jimish, private: true)